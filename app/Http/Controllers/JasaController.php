<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Jasa;
class JasaController extends Controller
{
    public function index()
    {
        return view('jasa',[
            "title"=>"JASA",
            "jasa"=>jasa::all()
        ]);
    }
}
