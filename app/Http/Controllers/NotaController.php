<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\nota;

class NotaController extends Controller
{
    public function index()
    {
        return view('nota',[
            "title"=>"NOTA",
            "nota"=>nota::all()
           
        ]);
    }
    public function show($id)
    {
        return view ('post',[
            "title"=>"DETAIL NOTA",
            "post"=>nota::find($id)
        ]);
    }
}
