<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Nota extends Model
{
    use HasFactory;

   // protected $fillable = ['kategori_nota','nama','alamat','berat','jasa','total','tanggal_nota'];
    protected $guarded = ['id'];
}
