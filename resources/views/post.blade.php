@extends('layouts.main')

@section('container')
<article>
    <h5>{{ $post->nama }}</h5>
    <h5>{{ $post->alamat }}</h5>
    <h5>{{ $post->berat}}</h5>
    <h5>{{ $post->jasa }}</h5>
    <h5>{{ $post->total}}</h5>
    <h5>{{ $post->tanggal_nota}}</h5>
    <h5>{{ $post->kategori_nota}}</h5>
</article>

<a href="/nota">Kembali</a>
@endsection