<?php

use App\Models\jasa;
use App\Models\nota;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\JasaController;
use App\Http\Controllers\NotaController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\ForgotController;
use App\Http\Controllers\RegisterController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [LoginController::class, 'index']);
Route::get('/register', [RegisterController::class, 'index']);
Route::get('/forgot', [ForgotController::class, 'index']);
Route::get('/jasa', [JasaController::class,'index'] );
Route::get('/nota', [NotaController::class,'index'] );
Route::get('nota/{slug}', [NotaController::class,'show']);
   



Route::get('/home', function () {
    return view('home',[
        "title"=>"HOME"
    ]);
   
});

Route::get('/profil', function () {
    return view('profil',[
        "title"=>"PROFIL",
       "nama"=>"Nama : Yozza amandha",
       "email"=>"Email : yozzaamandha1105@gmail.com",
       "hp"=>"Nomor Handphone : 082285329856",
      "alamat"=>"Alamat : Planet Bumi",
      "image"=>"male.png"

    ]);
});
